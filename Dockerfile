FROM python:alpine
WORKDIR /app
COPY server .
CMD ["python", "app.py"]
