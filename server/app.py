from csv import reader
from os import getenv


if __name__ == '__main__':
    path = getenv('DATA_PATH', 'data.csv')
    r = reader(open(path))
    for row in r:
        print(row)
