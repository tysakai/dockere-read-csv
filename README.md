# dockere-read-csv

Run app.py inside Docker to access the next data.csv.

## Pull and run

Use your [Personal Access Token](https://gitlab.com/profile/personal_access_tokens) instead of password on login.

```sh
$ docker login registry.gitlab.com
Username: ********@g.ecc.u-tokyo.ac.jp
Password: ********
WARNING! Your password will be stored unencrypted in /home/ubuntu/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
$ docker pull registry.gitlab.com/tysakai/dockere-read-csv/master
$ docker run registry.gitlab.com/tysakai/dockere-read-csv/master
['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']
```

## Build and run

```sh
$ git clone git@gitlab.com:tysakai/dockere-read-csv.git
$ docker build -t docker-read-csv .
$ docker run docker-read-csv
['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']
```
